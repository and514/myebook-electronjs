
from flask import Flask, request, jsonify

app = Flask(__name__)


@app.route('/add_folder', methods=['POST'])
def add_folder():
    if not request.json or 'folder' not in request.json:
        return jsonify({'error': 'There is no folder name in the request'}), 400
    folder_name = request.json['folder']
    print('folder=', folder_name)
    return jsonify({'folder': folder_name}), 201


if __name__ == '__main__':
    app.run()
