import logging
import os
from logging.handlers import TimedRotatingFileHandler

# from raven import Client
# from raven.handlers.logging import SentryHandler


LOG_FORMAT_DEFAULT = '%(levelname)s,  %(asctime)s,  module = %(module)s,  %(message)s'
LOG_FORMAT_SIMPLE = '%(levelname)s,  %(asctime)s,  %(message)s'
LOG_FORMAT_EXCEPTION = '%(levelname)s,  %(asctime)s,  %(filename)s,  %(pathname)s,  %(message)s, %(exception_info)s'
logger_dict = {}


def get_logger(logger_name, base_dir, app_config, log_format=LOG_FORMAT_DEFAULT):

    logger = logging.getLogger(logger_name)

    if logger_name not in logger_dict:

        if app_config.log_dir.startswith('/'):
            log_file_path = os.path.join(app_config.log_dir, f'{logger_name}.log')
        else:
            log_file_path = os.path.join(base_dir, app_config.log_dir, f'{logger_name}.log')
        handler = TimedRotatingFileHandler(log_file_path, backupCount=10)
        handler.setLevel(logging.DEBUG)
        handler.setFormatter(logging.Formatter(log_format))
        logger.addHandler(handler)

        # create console handler
        if app_config.is_debug_mode:
            ch = logging.StreamHandler()
            ch.setLevel(logging.DEBUG)
            ch.setFormatter(logging.Formatter(log_format))
            logger.addHandler(ch)

        # # create sentry handler
        # if app_config.sentry_dsn:
        #     client = Client(app_config.sentry_dsn)
        #     handler = SentryHandler(client)
        #     handler.setLevel(logging.ERROR)
        #     handler.setFormatter(logging.Formatter(log_format))
        #     logger.addHandler(handler)

        logging.basicConfig(
            filename=log_file_path,
            format=log_format,
            level=logging.INFO,
        )
        logger_dict[logger_name] = logger

    return logger
