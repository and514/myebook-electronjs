import pendulum


class Duration:
    def __init__(self):
        self.dt = pendulum.utcnow()

    @property
    def value(self):
        return pendulum.utcnow() - self.dt

    def __str__(self):
        return str(self.value.as_timedelta())