
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from src.settings import app_config

url = app_config.db.url

# create an engine
engine = create_engine(url)

# create a configured "Session" class
Session = sessionmaker(bind=engine)

# create a Session
# session = Session()
