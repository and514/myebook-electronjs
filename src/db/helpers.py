import importlib
import inspect
import os

from sqlalchemy import MetaData

from src.db.meta import Base
from src.settings import APPS, BASE_DIR, MODELS_FILE_NAME


def combine_metadata(*args):
    m = MetaData()
    for metadata in args:
        for t in metadata.tables.values():
            t.tometadata(m)
    return m


def add_metadata_models(metadata_model_list: list, name_module: str) -> list:
    imported_module = importlib.import_module(name_module)
    models_metadata = [
        obj.metadata for _, obj in inspect.getmembers(imported_module)
        if inspect.isclass(obj) and obj.__module__ == name_module and Base in obj.__bases__]
    metadata_model_list.extend(models_metadata)
    return metadata_model_list


def get_metadata_all_models(apps=APPS):
    metadata_model_list = []
    for app in apps:
        # file - models
        file_path = os.path.join(BASE_DIR, 'src', app.replace('.', '/'), '.'.join((MODELS_FILE_NAME, 'py')))
        if os.path.isfile(file_path):
            name_module = '.'.join(('src', app, MODELS_FILE_NAME))
            metadata_model_list = add_metadata_models(metadata_model_list, name_module)
        # dir - models
        dir_path = os.path.join(BASE_DIR, 'src', app.replace('.', '/'), MODELS_FILE_NAME)
        if os.path.isdir(dir_path):
            for top, dirs, files in os.walk(dir_path):
                for file_name in filter(lambda x: x != '__init__.py' and x.split('.')[-1] == 'py', files):
                    name_module = '.'.join(('src', app, MODELS_FILE_NAME, file_name.split('.')[0]))
                    metadata_model_list = add_metadata_models(metadata_model_list, name_module)
    return metadata_model_list


def get_target_metadata():
    metadata_list = get_metadata_all_models()
    if metadata_list:
        return combine_metadata(*metadata_list)
    else:
        return None
