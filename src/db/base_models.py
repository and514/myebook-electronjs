import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.ext.mutable import MutableDict


class HistoryModelMixin:
    # func.statement_timestamp() returns the start time of the current
    # statement (not start time of current transaction). Available only for PostgreSQL.
    created = sa.Column(sa.DateTime(timezone=True), server_default=sa.func.statement_timestamp())
    updated = sa.Column(sa.DateTime(timezone=True), server_default=sa.func.statement_timestamp(),
                        onupdate=sa.func.statement_timestamp())


class SettingModelMixin:

    name = sa.Column(sa.String(250), nullable=False)
    value = sa.Column(MutableDict.as_mutable(JSONB), nullable=False, default=sa.text("'{}'::jsonb"))

    @classmethod
    def get_value(cls, db_session, name: str) -> dict:
        st_model = db_session.query(cls).filter(cls.name == name).first()
        return st_model and st_model.value or {}

    @classmethod
    def set_value(cls, db_session, name: str, value: dict):
        st_model = db_session.query(cls).filter(cls.name == name).first()
        if st_model:
            st_model.value = value
        else:
            # noinspection PyArgumentList
            db_session.add(cls(name=name, value=value))
