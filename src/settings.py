import os

from src.helpers.config import get_config
from src.helpers.logger import get_logger

APP_NAME = 'ebook'

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

app_config = get_config(BASE_DIR, APP_NAME)
logger = get_logger(APP_NAME, BASE_DIR, app_config)

TITLES_DIR = os.path.join(BASE_DIR, app_config.load_info.title_dir)

# models: folders or files
MODELS_FILE_NAME = 'models'

APPS = (
    'book_info',
)
