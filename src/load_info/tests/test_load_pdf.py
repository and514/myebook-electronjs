import os

import pytest

from src.load_info.load_info_pdf import load_info_pdf
from src.settings import TITLES_DIR


def test_load_pdf_google():
    dir_name_test = 'test_source'
    filename_test = 'The Purpose-Driven Life_ What on Earth Am I Here For_ ( PDFDrive.com ).pdf'
    file_path_test = os.path.join(os.path.dirname(os.path.abspath(__file__)), dir_name_test, filename_test)

    # book_info
    # Сторонний сервис не патчим, нужно проверять его работоспособность
    book_info = load_info_pdf(file_path_test)
    assert book_info.get('error') is None
    assert book_info.get('page_count') == 340
    assert book_info.get('isbn') == ['0-310-25482-5']
    info_fields = ('file_path', 'page_count', 'isbn', 'title_file_path', 'title', 'creator', 'description', 'language')
    assert all([x in book_info for x in info_fields])

    # title
    png_file_name = os.path.join(TITLES_DIR, f'{".".join(filename_test.split(".")[:-1])}.png')
    assert book_info.get('title_file_path') == png_file_name
    assert os.path.isfile(png_file_name)
