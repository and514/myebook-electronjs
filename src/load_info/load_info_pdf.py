import os
import re
import xml.etree.ElementTree as Et

# noinspection PyPackageRequirements
import fitz
import requests

from src.settings import logger, TITLES_DIR

PATTERN_ISBN_1 = re.compile(r'ISBN[\D]+([\d\-]+)')
PATTERN_ISBN_2 = re.compile(r'ISBN-13[\D]+([\d\-]+)')

URL_SEARCH_GOOGLE = 'http://www.google.com/books/feeds/volumes/?q=ISBN'
XML_TEMPLATE_W3 = '{http://www.w3.org/2005/Atom}'
XML_TEMPLATE_PURL = '{http://purl.org/dc/terms}'


def save_title(doc: fitz.Document, file_name: str) -> str:
    """ Сохраняет обложку как картинку и возвращает имя файла """
    page = doc[0]
    # уменьшить в два раза
    mat = fitz.Matrix(0.5, 0.5)
    pix = page.getPixmap(mat)
    png_file_name = os.path.join(TITLES_DIR, f'{".".join(file_name.split("/")[-1].split(".")[:-1])}.png')
    pix.writePNG(png_file_name)
    return png_file_name


def get_isbn(doc: fitz.Document) -> (list, None):
    """ Находит и возвращает ISBN """
    for i, page in enumerate(doc):
        found = PATTERN_ISBN_1.findall(page.getText()) + PATTERN_ISBN_2.findall(page.getText())
        result = list(filter(lambda x: len(x) >= 13, set(found)))
        if result:
            return result
        if i > 5:
            break

    return None


def get_book_info_from_google(isbn: str) -> (dict, None):
    """ Находит информацию о книге из сервиса GOOGLE и возвращает в виде словаря """
    resp = requests.get(f'{URL_SEARCH_GOOGLE}<{isbn}>')
    if resp.ok:
        root = Et.fromstring(resp.text)
        entry_list = root.findall(f'{XML_TEMPLATE_W3}entry')
        if entry_list:
            entry = entry_list[0]
            return {
                'title': entry.find(f'{XML_TEMPLATE_W3}title').text,
                'creator': entry.find(f'{XML_TEMPLATE_PURL}creator').text,
                'description': entry.find(f'{XML_TEMPLATE_PURL}description').text,
                'language': entry.find(f'{XML_TEMPLATE_PURL}language').text,
            }
    return None


def load_info_pdf(file_path: str) -> (dict, None):
    """ Находит информацию о книге и возвращает в виде словаря """
    info = {}
    # noinspection PyUnresolvedReferences
    pdf_doc = fitz.open(file_path)
    # metadata = pdf_doc.metadata
    try:
        # ISBN
        isbn = get_isbn(pdf_doc)
        if not isbn:
            return {'error': f'{file_path}, ERROR: Не найден ISBN'}

        # Title
        title_file_path = save_title(pdf_doc, file_path)
        info = {
            'file_path': file_path,
            'page_count': pdf_doc.pageCount,
            'isbn': isbn,
            'title_file_path': title_file_path,
        }

        book_info = get_book_info_from_google(isbn[0])
        if not book_info:
            info.update({'error': f'{file_path}, ISBN: {isbn}, ERROR: Не найдена книга в {URL_SEARCH_GOOGLE}'})
        else:
            info.update(book_info)

        # TODO найти содержание, автор, издательский дом, год издания, редакция

    except ValueError as e:
        info.update({'error': str(e)})
    finally:
        pdf_doc.close()

    return info
